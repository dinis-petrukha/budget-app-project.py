import re

class Category:
	def __init__(self, category):
		self.ledger = []
		self.category = category

	def __str__(self):
		output = self.category.center(30, '*') + '\n'
		for row in self.ledger:
			description = row["description"][:23].ljust(23)
			description += str(f"{row['amount']:.2f}").rjust(7)
			output += description + '\n'
		output += 'Total: ' + str(self.get_balance())
		return output

	def deposit(self, amount, description=""):
		self.ledger.append({"amount": amount, "description": description})

	def get_balance(self):
		total = 0
		for history in self.ledger:
			total += history['amount']
		return total

	def withdraw(self, amount, description=""):
		if self.check_funds(amount):
			self.ledger.append({"amount": -amount, "description": description})
			return True
		return False

	def transfer(self, amount, category):
		if self.withdraw(amount, f"Transfer to {category.category}"):
			category.deposit(amount, f"Transfer from {self.category}")
			return True
		return False

	def check_funds(self, amount):
		return amount <= self.get_balance()

def create_spend_chart(categories):
	pass

food = Category("Food")
food.deposit(1000, "deposit")
food.withdraw(10.15, "groceries")
food.withdraw(15.89, "restaurant and more food for dessert")
clothing = Category("Clothing")
food.transfer(50, clothing)
print(food)